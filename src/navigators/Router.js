import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation';
import Login from '../containers/login/login';
import Register from '../containers/register/register';
import HomePage from '../containers/homepage/homepage';
import CarParking from '../containers/homepage/carparking';
import NearParkingPlace from '../containers/homepage/nearParkingPlace';
import ParkingPlace from '../containers/homepage/parkingPlace';
import Booking from '../containers/booking/booking';
import Direction from '../containers/direction/direction';
import History from '../containers/history/history';
import IndoorMap from '../containers/indoorMap/indoorMap';
import Support from '../containers/homepage/support';

const screens = {
    Login: {
        screen: Login,
        navigationOptions: {
            headerShown: false,
        },
    },
    Register: {
        screen: Register,
        navigationOptions: {
            headerShown: false,
        },
    },
    HomePage: {
        screen: HomePage,
        navigationOptions: {
            headerShown: false
        }
    },
    CarParking: {
        screen: CarParking,
        navigationOptions: {
            headerShown: false
        }
    },
    NearParkingPlace: {
        screen: NearParkingPlace,
        navigationOptions: {
            headerShown: false
        }
    },
    ParkingPlace: {
        screen: ParkingPlace,
        navigationOptions: {
            headerShown: false
        }
    },
    Booking: {
        screen: Booking,
        navigationOptions: {
            headerShown: false
        }
    },
    Direction: {
        screen: Direction,
        navigationOptions: {
            headerShown: false
        }
    },
    History: {
        screen: History,
        navigationOptions: {
            headerShown: false
        }
    },
    IndoorMap: {
        screen: IndoorMap,
        navigationOptions: {
            headerShown: false
        }
    },
    Support: {
        screen: Support,
        navigationOptions: {
            headerShown: false
        }
    }
};
const stack = createStackNavigator(screens);
export default createAppContainer(stack);