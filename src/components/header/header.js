import React from 'react';
import {
    View,
    Text,
    ScrollView,
    Platform,
    UIManager,
    LayoutAnimation,
    TouchableOpacity,
    Image,
    ActivityIndicator,
    RefreshControl,
    ImageBackground,
    Dimensions
} from 'react-native';

import styles from '../../styles/Header';
import Icon from 'react-native-vector-icons/MaterialIcons';

if (Platform.OS === 'android' && UIManager.setLayoutAnimationEnabledExperimental) {
    UIManager.setLayoutAnimationEnabledExperimental(true);
}

export default class Header extends React.Component {
    render() {
        return (
            <View style={styles.header}>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('HomePage')} style={styles.logoBtn}>
                    <Image style={styles.logo} source={require('../../assets/img/ic_launcher.png')} />
                </TouchableOpacity>
            </View>
        );
    }
}