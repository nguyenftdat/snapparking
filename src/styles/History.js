export default {
    container: {
        paddingTop: 8
    },
    block: {
        margin: 16,
        marginTop: 8,
        marginBottom: 8,
        padding: 16,
        backgroundColor: '#rgba(44, 62, 80, 0.2)',
        borderRadius: 5,
        flexDirection: 'row',
        borderLeftWidth: 3,
        borderColor: 'rgba(0, 173, 181, 1)'
    },
    txt: {
        // color: '#eeeeee',
        fontFamily: 'UTM-Swiss-Condensed',
        marginBottom: 4,
        fontSize: 16,
        color: '#333'
    },
    leftBlock: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#eeeeee',
        padding: 8,
        borderRadius: 5
    },
    rightBlock: {
        marginLeft: 16
    },
    boldTxt: {
        // color: '#eeeeee',
        fontFamily: 'UTM-Swiss-CondensedBold',
        fontSize: 24,
        color: '#333'
    },
    row: {
        flexDirection: 'row'
    },
    rightTxt: {
        marginLeft: 8
    }
}