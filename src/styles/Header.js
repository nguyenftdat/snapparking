const React = require("react-native");

import { Dimensions } from "react-native";

const { StyleSheet } = React;

export default {
    header: {
        paddingTop: 25,
        paddingBottom: 8,
        // alignItems: 'center',
        flexDirection: 'row',
        // marginLeft: 16,
        backgroundColor: 'rgba(238, 238, 238, 0.6)'
    },
    icon: {
        position: 'absolute',
        marginLeft: 16,
        marginTop: 25,
        zIndex: 1000
    },
    logo: {
        width: 50,
        height: 50,
    },
    logoBtn: {
        flex: 1,
        alignItems: 'center',
    }
}