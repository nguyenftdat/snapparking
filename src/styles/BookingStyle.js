import Dimension, { Dimensions } from 'react-native';

var deviceWidth = Dimensions.get('window').width;
var deviceHeight = Dimensions.get('window').height;

export default {
    container: {
        // padding: 8, 
        backgroundColor: 'rgba(57, 62, 70, 0.2)',
        
    },
    mapView: {
        height: deviceHeight*0.3,
        marginBottom: 16,
        marginLeft: 16,
        marginRight: 16,
    },
    infoContainer: {
        marginTop: 32,
        marginBottom: 16,
        borderRadius: 5,
        padding: 16,
        backgroundColor: 'rgba(238, 238, 238, 0.5)',
        marginLeft: 16,
        marginRight: 16
    },
    name: {
        fontFamily: 'UTM-Swiss-CondensedBold',
        fontSize: 24,
        marginBottom: 8
    },
    row: {
        flexDirection: 'row',
        marginBottom: 8
    },
    leftTxt: {
        fontFamily: 'UTM-Swiss-CondensedBold',
        width: deviceWidth*0.35,
        marginBottom: 4
    },
    rightTxt: {
        fontFamily: 'UTM-Swiss-Condensed',
        width: deviceWidth*0.5,
        marginBottom: 4,
        marginRight: 24
    },
    area: {
        marginBottom: 8,
        borderRadius: 5,
        padding: 16,
        backgroundColor: 'rgba(238, 238, 238, 0.5)',
        marginLeft: 16,
        marginRight: 16
    },
    txt: {
        fontFamily: 'UTM-Swiss-Condensed',
        marginBottom: 8
    },
    whiteTxt: {
        fontFamily: 'UTM-Swiss-Condensed',
        color: '#eeeeee',
        // marginBottom: 8
    },
    emptySpace: {
        padding: 8,
        backgroundColor: 'rgba(0, 173, 181, 0.8)',
        borderRadius: 5,
        marginRight: 16
    },
    filledSpace: {
        padding: 8,
        backgroundColor: '#e7305b',
        borderRadius: 5,
        marginRight: 16
    },
    selectedSpace: {
        padding: 8,
        backgroundColor: '#a2de96',
        borderRadius: 5,
        marginRight: 16
    },
    bookingBtn: {
        backgroundColor: 'rgba(0, 173, 181, 0.9)',
        padding: 16,
        color: '#eeeeee',
        width: deviceWidth*0.5,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 150,
        marginBottom: 16
    },
    noBookingBtn: {
        backgroundColor: '#333',
        padding: 16,
        color: '#eeeeee',
        width: deviceWidth*0.5,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 150,
        marginBottom: 16,
        // opacity: 0.5
    },
    verifyTxt: {
        fontFamily: 'UTM-Swiss-CondensedBold',
        color: '#eeeeee',
        fontSize: 20
    }
}