const React = require("react-native");

import { Dimensions } from "react-native";

const { StyleSheet } = React; 

export default {
    mapView: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,

    },
    searchView: {
        width: Dimensions.get('window').width -48,
        flexDirection: 'row',
        backgroundColor: 'rgba(0, 173, 181, 0.8)',
        // marginTop: 60,
        marginLeft: 24,
        marginRight: 24,
        borderTopRightRadius: 50,
        borderBottomRightRadius: 50,
        borderTopLeftRadius: 50,
        borderBottomLeftRadius: 50,
        paddingTop: 8,
        paddingBottom: 8,
        paddingLeft: 8,
        // position: 'absolute',
    },
    searchTxt: {
        color: '#fff',
        marginLeft: 16, 
        fontSize: 22,
        maxWidth: Dimensions.get('window').width -108,
        // maxWidth: 100,
        fontFamily: 'UTM-Swiss-Condensed',
        
    },
    bottomView: {
        height: 125,
        bottom: 0,
        alignItems: 'center',
    },
    fabButton: {
        // marginTop: 15,
        color: '#eeeeee'
    },
    clearButton: {
        opacity: 0.4,
        justifyContent: 'center',
        marginTop: 13,
        marginBottom: 12,
        backgroundColor: '#fff',
        borderRadius: 500
    }
}