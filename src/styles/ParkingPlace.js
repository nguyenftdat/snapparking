
export default {
    container: {

    },
    title: {
        fontFamily: 'UTM-Swiss-Condensed',
        fontSize: 30,
        color: '#eeeeee',
        marginBottom: 8
    },
    block: {
        backgroundColor: '#rgba(44, 62, 80, 0.5)',
        padding: 16,
        margin: 16,
        borderRadius: 5
        // flex: 1
    },
    icon: {
        width: 50,
        height: 50,
        resizeMode: 'contain'
    },
    row: {
        flexDirection: 'row',
        marginBottom: 16
    },
    infoFormTextInput: {
        marginLeft: 16,
        fontFamily: 'UTM-Swiss-Condensed',
        fontSize: 18,
        borderBottomWidth: 1,
        borderBottomColor: '#eeeeee',
        flex: 1,
        color: '#eeeeee'
    },
    createBtn: {
        marginTop: 16,
        padding: 16,
        paddingLeft: 24,
        paddingRight: 24,
        backgroundColor: 'rgba(0, 173, 181, 0.8)',
        borderRadius: 10
    },
    createTxt: {
        fontFamily: 'UTM-Swiss-Condensed',
        fontSize: 20,
        color: '#eeeeee'
    }
}