const React = require("react-native");

import { Dimensions } from "react-native";

const { StyleSheet } = React;  
export default {
    container: {
        padding: 16
    },
    infoContainer: {
        // height: Dimensions.get('window').height*0.35,
        // marginTop: 8,
        backgroundColor: 'rgba(238, 238, 238, 0.5)',
        borderRadius: 5,
        padding: 24, 
        marginBottom: 16,
    },
    qrcodeContainer: {
        // justifyContent: 'center',
        // alignItems: 'center'
    },
    infoBlock: {
        flexDirection: 'row',
    },
    
    leftTxt: {
        fontFamily: 'UTM-Swiss-Condensed',
        width: Dimensions.get('window').width*0.2,
        marginBottom: 8
    },
    rightTxt: {
        fontFamily: 'UTM-Swiss-Condensed',
        width: Dimensions.get('window').width*0.7 - 40,
        marginBottom: 8
    },
    titleTxt: {
        fontFamily: 'UTM-Swiss-CondensedBold',
        fontSize: 16,
        marginBottom: 8
    },
    cancelBtn: {
        paddingTop: 16,
        paddingBottom: 16,
        paddingLeft: 32,
        paddingRight: 32,
        marginTop: 16,
        backgroundColor: '#e7305b',
        // position: 'absolute',
        // bottom: 24,
        // alignSelf: 'center',
        borderRadius: 5
    },
    cancelTxt: {
        fontFamily: 'UTM-Swiss-CondensedBold',
        color: '#eeeeee'
    },
    indoorMap: {
        marginLeft: 24,
        paddingTop: 16,
        paddingBottom: 16,
        paddingLeft: 32,
        paddingRight: 32,
        marginTop: 16,
        backgroundColor: 'rgb(0, 173, 181)',
        // position: 'absolute',
        // bottom: 24,
        // alignSelf: 'center',
        borderRadius: 5
    },
    indoorTxt: {
        fontFamily: 'UTM-Swiss-CondensedBold',
        color: '#eeeeee'
    },
}