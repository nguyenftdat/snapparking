const React = require("react-native");
import { Dimensions } from "react-native";

const { StyleSheet } = React;
var deviceWidth = Dimensions.get("window").width;
var deviceHeight = Dimensions.get("window").height;

export default {
    container: {
        paddingTop: 8
    },
    block: {
        justifyContent: 'center',
        alignSelf: 'center',
        flex: 1,
        alignItems: 'center',
    },
    txt: {
        fontFamily: 'UTM-Swiss-Condensed',
        marginBottom: 4,
        fontSize: 16,
        color: '#333'
    },
    supportInfoBlock: {
        backgroundColor: 'rgba(238, 238, 238, 0.5)',
        padding: 16,
        width: deviceWidth*0.7,
        borderRadius: 5
    }
}