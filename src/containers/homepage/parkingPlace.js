import React from 'react';
import {
    View,
    Text,
    Image,
    ImageBackground,
    TextInput,
    UIManager,
    LayoutAnimation,
    TouchableOpacity
} from 'react-native';
import styles from '../../styles/ParkingPlace';
import Header from '../../components/header/header';
import Icon from 'react-native-vector-icons/MaterialIcons';

if (Platform.OS === 'android' && UIManager.setLayoutAnimationEnabledExperimental) {
    UIManager.setLayoutAnimationEnabledExperimental(true);
}

export default class ParkingPlace extends React.Component {
    state = {
        address: '',
        time: '',
        price: '',
        expanded: false
    }
    render() {
        return (
            <ImageBackground source={require('../../assets/img/abstract_background_5.jpg')} style={{ flex: 1, resizeMode: 'contain' }}>
                <Header navigation={this.props.navigation}></Header>
                <View style={styles.container}>
                    <View style={styles.block}>
                        <TouchableOpacity
                            onPress={() => {
                                LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
                                this.setState({ expanded: !this.state.expanded });
                            }}>
                            <Text style={styles.title}>Tạo bãi đỗ xe</Text>
                        </TouchableOpacity>
                        {this.state.expanded &&
                            <View style={{ marginTop: 8 }}>
                                <View style={styles.row}>
                                    <Image style={styles.icon} source={require('../../assets/img/P-icon.png')}></Image>
                                    <TextInput placeholder="Tên"
                                        value={this.state.address}
                                        selectionColor='#eeeeee'
                                        onChangeText={(address) => this.setState({ address })}
                                        placeholderColor="#fff"
                                        style={styles.infoFormTextInput} />
                                </View>
                                <View style={styles.row}>
                                    <Image style={styles.icon} source={require('../../assets/img/address_icon.png')}></Image>
                                    <TextInput placeholder="Địa chỉ"
                                        value={this.state.address}
                                        selectionColor='#eeeeee'
                                        onChangeText={(address) => this.setState({ address })}
                                        placeholderColor="#fff"
                                        style={styles.infoFormTextInput} />
                                </View>
                                <View style={styles.row}>
                                    <Image style={styles.icon} source={require('../../assets/img/price_icon.png')}></Image>
                                    <TextInput placeholder="Giá"
                                        value={this.state.price}
                                        selectionColor='#eeeeee'
                                        onChangeText={(price) => this.setState({ price })}
                                        placeholderColor="#fff"
                                        style={styles.infoFormTextInput} />
                                </View>
                                <View style={styles.row}>
                                    <Image style={styles.icon} source={require('../../assets/img/operation-time-icon.png')}></Image>
                                    <TextInput placeholder="Thời gian hoạt động"
                                        value={this.state.time}
                                        selectionColor='#eeeeee'
                                        onChangeText={(time) => this.setState({ time })}
                                        placeholderColor="#fff"
                                        style={styles.infoFormTextInput} />
                                </View>
                                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                    <TouchableOpacity style={styles.createBtn}>
                                        <Text style={styles.createTxt}>Tạo</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>}
                    </View>
                </View>
            </ImageBackground>
        );
    }
}