import React from 'react';
import {
    View,
    TextInput,
    Image,
    TouchableOpacity,
    ActivityIndicator,
    Dimensions,
    ImageBackground,
    LayoutAnimation,
    Text,
    Platform,
    UIManager,
    AsyncStorage
} from 'react-native';
import MapView, { Marker, Polyline } from 'react-native-maps';
// import MapView from 'expo';
import Polylines from '@mapbox/polyline';
import * as Permissions from 'expo-permissions';
import styles from '../../styles/CarParkingStyle'
import { Button } from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialIcons';
import * as Location from 'expo-location';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import * as Constants from '../../common/constants';
import Geocode from "react-geocode";
import Header from '../../components/header/header';

if (Platform.OS === 'android' && UIManager.setLayoutAnimationEnabledExperimental) {
    UIManager.setLayoutAnimationEnabledExperimental(true);
};

export default class CarParking extends React.Component {

    state = {
        latitude: null,
        longitude: null,
        address: null,
        // locations: locations,
        isLoading: true,
        // expanded: false
    }
    async componentDidMount() {
        const { status } = await Permissions.getAsync(Permissions.LOCATION)
        if (status !== 'granted') {
            const response = await Permissions.askAsync(Permissions.LOCATION)
        }
        navigator.geolocation.getCurrentPosition(
            ({ coords: { latitude, longitude } }) => this.setState({ latitude, longitude }, () => this.getAddress(latitude, longitude), this.getMarker(latitude, longitude)),
            (error) => console.log('Error', error)
        )
    }

    getMarker = async (lat, lng) => {
        let param = await AsyncStorage.getItem('@storage_param');
        param = JSON.parse(param);
        fetch(Constants.BASE_URL + '/search/coord/', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: param.token
            },
            body: JSON.stringify({
                lat: lat,
                lon: lng,
                distance: '5km',
                mode: 'distance',
                size: 5,
                index: 0,
                order: 'asc'
            })
        })
            .then(respond => respond.json())
            .then(respondJson => {
                console.log(respondJson)
                this.setState({
                    coordList: respondJson
                },
                    function () {
                        console.log(respondJson)
                    })
            })
            .catch(error => {
                console.log(error)
            })
    }

    getAddress = async (lat, lng) => {
        // set Google Maps Geocoding API for purposes of quota management. Its optional but recommended.
        Geocode.setApiKey(Constants.API_key);

        // set response language. Defaults to english.
        Geocode.setLanguage("en");

        // set response region. Its optional.
        // A Geocoding request with region=es (Spain) will return the Spanish city.
        Geocode.setRegion("es");

        // Enable or disable logs. Its optional.
        Geocode.enableDebug();
        // Get address from latidude & longitude.
        Geocode.fromLatLng(lat, lng).then(
            response => {
                const address = response.results[0].formatted_address;
                // console.log(address);
                this.setState({
                    address: address
                })
            },
            error => {
                console.error(error);
            }
        );
    }

    getCoord = async (address) => {
        Geocode.fromAddress(address).then(
            response => {
                const { lat, lng } = response.results[0].geometry.location;
                this.props.navigation.navigate('NearParkingPlace', { lat: lat, lng: lng })
            },
            error => {
                console.error(error);
            }
        );
    }

    search(address) {
        fetch(Constants.BASE_URL + '/search/address/', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                address: this.state.country
            })
        })
            .then(respond => respond.json())
            .then(respondJson => {

                this.setState({
                    dataSource: respondJson
                },
                    function () {
                        // console.log(respondJson)
                    })
            })
            .catch(error => {
                console.log(error)
            })
        // console.log('SEARCH', address)
    }

    render() {
        const {
            latitude,
            longitude,
            address,
            coordList
        } = this.state
        if (address && coordList) {
            return (
                <ImageBackground source={require('../../assets/img/abstract_background_5.jpg')} style={{ flex: 1, resizeMode: 'contain' }}>
                    <Header navigation={this.props.navigation} lat={this.state.latitude} lng={this.state.longitude}></Header>
                    <View>
                        <MapView style={styles.mapView}
                            showsUserLocation
                            showsMyLocationButton={true}
                            initialRegion={{
                                latitude,
                                longitude,
                                latitudeDelta: 0.0922,
                                longitudeDelta: 0.0421,
                            }}>
                            {/* {this.renderMarkers()}
                            <Polyline
                                strokeWidth={2}
                                strokeColor="red"
                                coordinates={coords}
                            /> */}
                            {coordList && coordList.result.map(marker => (
                                <Marker coordinate={{ latitude: marker.lat, longitude: marker.lon }}
                                    title={marker.location_name}>
                                </Marker>
                            ))}
                        </MapView>
                        <View style={{ position: 'absolute', marginTop: 16 }}>
                            <View style={styles.searchView}>
                                <GooglePlacesAutocomplete
                                    styles={styles.searchTxt, {
                                        textInputContainer: {
                                            backgroundColor: 'transparent',
                                            borderTopWidth: 0,
                                            borderBottomWidth: 0,
                                        },
                                        textInput: {
                                            marginLeft: 0,
                                            marginRight: 8,
                                            // height: 38,
                                            // borderRadius: 5,
                                            color: '#fff',
                                            fontSize: 16,
                                            backgroundColor: 'transparent',
                                            fontFamily: 'UTM-Swiss-Condensed',
                                            // selectionColor: '#fff',
                                            color: '#fff'
                                        },
                                        predefinedPlacesDescription: {
                                            color: '#fff',
                                        },
                                        container: {
                                            marginRight: 8
                                        },
                                        separator: {
                                            borderColor: '#fff'
                                        },
                                        poweredContainer: {
                                            display: 'none'
                                        },
                                        predefinedPlacesDescription: {
                                            color: '#fff'
                                        },
                                        description: {
                                            color: '#fff',
                                            fontFamily: 'UTM-Swiss-Condensed',
                                        },
                                    }}
                                    getDefaultValue={() => {
                                        return this.state.address; // text input default value
                                    }}

                                    onPress={(data, details = null) => {
                                        // 'details' is provided when fetchDetails = true
                                        this.getCoord(data.description);
                                        this.setState({
                                            address: data.description
                                        })
                                    }}
                                    query={{
                                        key: Constants.API_key,
                                        language: 'en',
                                        components: 'country:vn',
                                    }}
                                    textInputProps={{
                                        clearButtonMode: 'never',
                                        ref: input => {
                                            this.textInput = input;
                                        }
                                    }}
                                    renderRightButton={() => (
                                        <TouchableOpacity
                                            style={styles.clearButton}
                                            onPress={() => {
                                                this.textInput.clear();
                                            }}
                                        >
                                            <Icon
                                                name="clear"
                                                size={20}
                                                style={styles.fabButton}
                                            />
                                        </TouchableOpacity>
                                    )}
                                />
                                <TouchableOpacity onPress={() => this.getCoord(address)}>
                                    <Icon name='search' size={24} color={'#fff'} style={{ justifyContent: 'center', marginRight: 16, marginTop: 8 }} />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </ImageBackground>
            );
        }
        return (
            <View style={{ flex: 1, padding: 20 }}>
                <ActivityIndicator />
            </View>
        );
    }
}