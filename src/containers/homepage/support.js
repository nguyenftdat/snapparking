import React from 'react';
import {
    View,
    Text,
    Image,
    ImageBackground,
    TextInput,
    UIManager,
    LayoutAnimation,
    TouchableOpacity,
    AsyncStorage,
    ActivityIndicator,
} from 'react-native';
import styles from '../../styles/Support';
import Header from '../../components/header/header';
import Icon from 'react-native-vector-icons/MaterialIcons';
import * as Constants from '../../common/constants';
import { ScrollView } from 'react-native-gesture-handler';


export default class Support extends React.Component {
    render() {
        return (
            <ImageBackground source={require('../../assets/img/abstract_background_5.jpg')} style={{ flex: 1, resizeMode: 'contain' }}>
                <Header navigation={this.props.navigation}></Header>
                <View style={styles.block}>
                    <TouchableOpacity style={styles.supportInfoBlock}>
                        <Text style={styles.txt}>Mọi thắc mắc, phản hồi xin vui lòng đóng góp tại: sp.snapsmartparking@gmail.com</Text>
                    </TouchableOpacity>
                </View>
            </ImageBackground>
        );
    }
}