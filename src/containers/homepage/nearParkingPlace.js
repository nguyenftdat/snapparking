import React from 'react';
import {
    View,
    Text,
    ScrollView,
    Platform,
    UIManager,
    LayoutAnimation,
    TouchableOpacity,
    Image,
    ActivityIndicator,
    RefreshControl,
    ImageBackground,
    Dimensions,
    AsyncStorage
} from 'react-native';
import styles from '../../styles/NearParkingPlaceStyle';
import * as Constants from '../../common/constants';
import Header from '../../components/header/header';
import Icon from 'react-native-vector-icons/MaterialIcons';

if (Platform.OS === 'android' && UIManager.setLayoutAnimationEnabledExperimental) {
    UIManager.setLayoutAnimationEnabledExperimental(true);
}


export default class NearParkingPlace extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            refreshing: false,
            // status: 'row',
            isLoading: true,
            lat: this.props.navigation.state.params.lat,
            lng: this.props.navigation.state.params.lng,
            // mode: this.props.navigation.state.params.mode || 'distance',
            expanded: false
        }

        this.searchCoords = this.searchCoords.bind(this);
    }

    componentDidMount() {
        this.searchCoords('distance');
    }

    _onRefresh = () => {
        this.setState({ refreshing: true });
        fetchData().then(() => {
            this.setState({ refreshing: false });
        });
    }

    selectionHandler = (idx) => {
        const { data } = this.state.dataSource;
        let arr = this.state.dataSource.map((item, index) => {
            if (idx == index) {
                item.isSelected = !item.isSelected
            }
            return { ...item }
        })
        this.setState({ dataSource: arr })
        LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
    }
    searchCoords = async (mode) => {
        let param = await AsyncStorage.getItem('@storage_param');
        param = JSON.parse(param);
        console.log(param);
        fetch(Constants.BASE_URL + '/search/coord/', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: param.token
            },
            body: JSON.stringify({
                lat: this.state.lat,
                lon: this.state.lng,
                distance: '5km',
                mode: mode,
                size: 10,
                index: 0,
                order: 'asc'
            })
        })
            .then(response => response.json())
            .then(responseJson => {
                responseJson.result.forEach(item => {
                    item.isSelected = false
                })
                this.setState(
                    {
                        isLoading: false,
                        dataSource: responseJson.result,
                    },
                );
            })
            .catch(error => {
                console.error(error);
            });
    }
    render() {
        const DURATION = 'duration';
        const PRICE = 'min_price';
        const DISTANCE = 'distance';
        if (this.state.isLoading) {
            return (
                <View style={{ flex: 1, padding: 20 }}>
                    <ActivityIndicator />
                </View>
            );
        }
        return (
            <View style={styles.container}>
                <ImageBackground source={require('../../assets/img/abstract_background_5.jpg')} style={{ flex: 1, resizeMode: 'contain', width: Dimensions.get('window').width }}>
                    <Header navigation={this.props.navigation}>
                    </Header>
                    <TouchableOpacity style={styles.icon}
                        onPress={() => {
                            LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
                            this.setState({ expanded: !this.state.expanded });
                        }}>
                        <Icon name='dehaze' size={50} color={'#eeeeee'} />
                    </TouchableOpacity>
                    {this.state.expanded &&
                        <View style={styles.filter}>
                            <TouchableOpacity style={styles.filterFirstItem}
                                onPress={() => this.searchCoords(DISTANCE)}>
                                <Text style={styles.filterTxt}>Khoảng cách</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.filterItem}
                                onPress={() => this.searchCoords(PRICE)}>
                                <Text style={styles.filterTxt}>Giá</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.filterLastItem}
                                onPress={() => this.searchCoords(DURATION)}>
                                <Text style={styles.filterTxt}>Thời gian di chuyển</Text>
                            </TouchableOpacity>
                        </View>
                    }
                    {this.state.dataSource.length > 0 &&
                        <ScrollView>
                            {this.state.dataSource.map((item, index) => (
                                <TouchableOpacity
                                    key={index}
                                    onPress={() => this.selectionHandler(index)}
                                    style={!item.isSelected ? styles.mainBlock_Row : styles.mainBlock_Col} >
                                    {!item.isSelected &&
                                        <View style={styles.leftBlock}>
                                            <Text style={styles.whiteText}>{'Price      ' + item.min_price + ' VND'}</Text>
                                            <Text style={styles.whiteText}>{'Status     ' + item.schedule.status}</Text>
                                        </View>}
                                    {!item.isSelected &&
                                        <View style={styles.rightBlock}>
                                            <Text style={styles.whiteText} numberOfLines={1}>{item.address}</Text>
                                            {item.schedule.open_time &&
                                                <Text style={styles.whiteText}>{item.schedule.open_time + ' - ' + item.schedule.close_time}</Text>
                                                || <Text style={styles.whiteText}>00:00 - 23:59</Text>}
                                        </View>}
                                    {item.isSelected &&
                                        <View style={styles.expandedInfo}>
                                            <View>
                                                <Image style={styles.imageParking} source={require('../../assets/img/uni.jpg')} />
                                                <Text style={styles.name}>{item.location_name}</Text>
                                                <Text style={styles.carText}>CARS</Text>
                                                <Text style={styles.carNumber}>{item.stat.EMPTY + '/' + Number(item.stat.EMPTY + item.stat.FILLED)}</Text>
                                            </View>
                                            <View style={[styles.subBlock, { backgroundColor: '#fff' }]}>
                                                <Text style={styles.ownerName}>{item.address}</Text>
                                            </View>
                                            <View>
                                                <TouchableOpacity
                                                    style={styles.bookingBtn}
                                                    onPress={() => this.props.navigation.navigate('Booking', { startLat: this.state.lat, startLng: this.state.lng, lat: item.lat, lng: item.lon, dataSource: item })}>
                                                    <Text style={styles.bookingTxt}>
                                                        XEM CHI TIẾT
                                            </Text>
                                                </TouchableOpacity>
                                            </View>
                                        </View>}
                                </TouchableOpacity>))}
                        </ScrollView>}
                    {this.state.dataSource.length == 0 &&
                        <View style={styles.noParking}>
                            <Text style={styles.noParkingTxt}> There is no parking nearby </Text>
                        </View>}
                </ImageBackground>
            </View>
        )
    }
}