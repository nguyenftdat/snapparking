import * as React from 'react';
import {
    View,
    Text,
    Image,
    TouchableOpacity,
    ImageBackground,
    ActivityIndicator,
    LayoutAnimation,
    TextInput,
    Platform,
    UIManager,
    AsyncStorage,
    ScrollView,
    Dimensions
} from 'react-native';
import styles from "../../styles/Homepage";
import Icon from 'react-native-vector-icons/MaterialIcons';
import * as Constants from '../../common/constants';

if (Platform.OS === 'android' && UIManager.setLayoutAnimationEnabledExperimental) {
    UIManager.setLayoutAnimationEnabledExperimental(true);
}
export default class Account extends React.Component {
    state = {
        isLoading: true,
        expanded: false,
        pwd: '',
        re_pwd: '',
        status: false
    }
    componentDidMount() {
        this.getInfo();
    }
    changePassword = async () => {
        const token = await AsyncStorage.getItem('@storage_token');
        fetch(Constants.BASE_URL + '/account/changepassword/', {
            method: 'PUT',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: token
            },
            body: JSON.stringify({
                password: this.state.pwd,
                re_password: this.state.re_pwd
            })
        })
            .then(response => response.json())
            .then(responseJson => {
                this.setState(
                    {
                        isLoading: false,
                        dataSource_: responseJson,
                        pwd: '',
                        re_pwd: '',
                    },
                    function () {
                        
                        // console.log(responseJson)
                        setTimeout(() => {
                            if (responseJson.message == 'Successed.') {
                                this.setState({
                                    message: 'Đổi mật khẩu thành công!',
                                    status: true,
                                    expanded: false
                                })

                            }
                            else if (responseJson.re_password){
                                this.setState({
                                    message: 'Xác nhận mật khẩu không chính xác.',
                                    status: true
                                })
                            }
                            else {
                                this.setState({
                                    message: 'Mật khẩu phải ít nhất 6 kí tự: số, chữ hoa, chữ thường.',
                                    status: true
                                })
                            }
                        }, 2000)
                        setTimeout(() => {
                            this.setState({
                                status: false
                            })
                        }, 5000)
                    }
                );
            })
            .catch(error => {
                console.error(error);
            });
    }

    getInfo = async () => {
        const token = await AsyncStorage.getItem('@storage_token');
        fetch(Constants.BASE_URL + '/account/userinfo/', {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: token
            }
        })
            .then(response => response.json())
            .then(responseJson => {
                this.setState(
                    {
                        isLoading: false,
                        dataSource: responseJson,
                    },
                    function () {

                    }
                );
            })
            .catch(error => {
                console.error(error);
            });
    }
    render() {
        if (this.state.isLoading) {
            return (
                <View style={{ flex: 1, padding: 20 }}>
                    <ActivityIndicator />
                </View>
            );
        }
        return (
            <ImageBackground source={require('../../assets/img/abstract_background_5.jpg')} style={{ flex: 1, resizeMode: 'contain' }}>
                <ScrollView style={[styles.scene, { height: Dimensions.get('window').height }]}>
                    <View style={styles.profileBlock}>
                        <View>
                            <Image style={styles.avt} source={require("../../assets/img/account.png")} />
                        </View>
                        <View style={styles.info}>
                            <Text style={styles.name}>{this.state.dataSource.first_name + ' ' + this.state.dataSource.last_name}</Text>
                            <View style={styles.infoItem}>
                                <Icon name='place' style={styles.icon} size={24} />
                                <Text style={styles.infoTxt}>HCMC, Vietnam</Text>
                            </View>
                            <View style={styles.infoItem}>
                                <Icon name='mail' style={styles.icon} size={24} />
                                <Text style={styles.infoTxt}>{this.state.dataSource.email}</Text>
                            </View>
                            <View style={styles.infoItem}>
                                <Icon name='smartphone' style={styles.icon} size={24} />
                                <Text style={styles.infoTxt}>096758358</Text>
                            </View>
                        </View>
                    </View>
                    {/* <View style={styles.row, {alignSelf: 'center'}}>
                <TouchableOpacity style={styles.manageProfileBtn}>
                    <Image style={styles.btnLogo} source={require('../../assets/img/white_manage-profile.png')} />
                    <Text style={styles.txt}>Manage Profile</Text>
                </TouchableOpacity>
            </View> */}
                    <View style={styles.row, { alignSelf: 'center' }}>
                        <TouchableOpacity style={styles.changePasswordBtn}
                            onPress={() => {
                                LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
                                this.setState({ expanded: !this.state.expanded });
                            }}>
                            <Image style={styles.btnLogo} source={require('../../assets/img/white_change-password.png')} />
                            <Text style={styles.txt}>Thay đổi mật khẩu </Text>
                        </TouchableOpacity>
                    </View>
                    {this.state.expanded &&
                        <View style={styles.changePwdBlock}>
                            <View style={styles.subBlock}>
                                <Text style={styles.subTxt}>Nhập mật khẩu mới</Text>
                                <TextInput placeholder="Mật khẩu mới"
                                    value={this.state.pwd}
                                    selectionColor='#eeeeee'
                                    onChangeText={(pwd) => this.setState({ pwd })}
                                    placeholderTextColor="#eee"
                                    secureTextEntry={true}
                                    style={styles.pwdFormTextInput} />
                            </View>
                            <View style={styles.subBlock}>
                                <Text style={styles.subTxt}>Nhập lại mật khẩu</Text>
                                <TextInput placeholder="Xác nhận"
                                    value={this.state.re_pwd}
                                    selectionColor='#eeeeee'
                                    onChangeText={(re_pwd) => this.setState({ re_pwd })}
                                    placeholderTextColor="#eee"
                                    style={[styles.pwdFormTextInput, { marginBottom: 8 }]}
                                    secureTextEntry={true} />
                            </View>
                            <TouchableOpacity style={styles.verifyPwdBtn}
                                onPress={() => this.changePassword()}>
                                <Text style={[styles.subTxt, { color: '#eeeeee' }]}>XÁC NHẬN</Text>
                            </TouchableOpacity>
                        </View>}
                </ScrollView>
                {this.state.status &&
                    <View style={styles.message}>
                        <Text style={[styles.subTxt, { color: '#eeeeee' }]}>{this.state.message || ' '}</Text>
                    </View>}
            </ImageBackground>
        )
    }
}