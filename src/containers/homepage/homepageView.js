import * as React from 'react';
import { View, Text, Image, TouchableOpacity, ImageBackground, AsyncStorage } from 'react-native';
import styles from "../../styles/Homepage";
import { useNavigation } from '@react-navigation/native';
import { LinearGradient } from 'expo-linear-gradient';
import * as Constants from '../../common/constants';

export default class CarParking extends React.Component {
    componentDidMount(){
        // this.getRoute();
    }
    getRoute = async () => {
        let param = await AsyncStorage.getItem('@storage_param');
            param = JSON.parse(param);
        if (param.status && param.status == true) {
            fetch(Constants.BASE_URL + '/parking/reservation/', {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: param.token
                }
            })
                .then(response => response.json())
                .then(responseJson => {
                    this.setState(
                        {
                            isLoading: false,
                            dataSource_: responseJson,
                        },
                        function () {
                            if (this.state.dataSource_) {
                                this.props.navigation.navigate('Direction');
                            }
                        }
                    );
                })
                .catch(error => {
                    console.error(error);
                });
        }
        else {
            this.props.navigation.navigate('CarParking');
        }
    }
    render() {
        return (
            <View style={styles.scene}>
                <ImageBackground source={require('../../assets/img/abstract_background_2.jpg')} style={{ flex: 1, resizeMode: 'contain' }}>
                    <View style={styles.logoView}>
                        <Image style={styles.logo} source={require("../../assets/img/ic_launcher.png")} />
                    </View>
                    <View style={{ flex: 1, justifyContent: 'center' }}>
                        <View style={styles.centerRow}>
                            <TouchableOpacity style={styles.carParkingBtn}
                                onPress={() => this.getRoute()}>
                                <Image style={styles.imgLogo} source={require('../../assets/img/white_carparking_ic.png')} />
                                <Text style={styles.carParkingTxt}>Car Parking</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.parkingPlaceBtn}
                                onPress={() => this.props.navigation.navigate('History')}>
                                <Image style={styles.imgLogo} source={require('../../assets/img/history_icon.png')} />
                                <Text style={styles.carParkingTxt}>History</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.centerRow}>
                            
                            <TouchableOpacity style={styles.supportBtn}
                                onPress={() => this.props.navigation.navigate('Support')}>
                                <Image style={styles.imgLogo} source={require('../../assets/img/white_support_ic.png')} />
                                <Text style={styles.supportTxt}>Support</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ImageBackground>
            </View>
        )
    }
}