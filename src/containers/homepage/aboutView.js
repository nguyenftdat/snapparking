import * as React from 'react';
import { View, Text, Image, TouchableOpacity, ImageBackground, AsyncStorage } from 'react-native';
import styles from "../../styles/Homepage";
import Icon from 'react-native-vector-icons/MaterialIcons';

export default class About extends React.Component {
    logOut = async () => {
        await AsyncStorage.removeItem('@storage_param');
        this.props.navigation.navigate('Login');
    }
    render() {
        return (
            <View style={styles.scene}>
                <ImageBackground source={require('../../assets/img/abstract_background_5.jpg')} style={{ flex: 1, resizeMode: 'contain' }}>
                    <View style={styles.logoBlock}>
                        <Image style={styles.roundLogo} source={(require('../../assets/img/ic_launcher_round.png'))} />
                        <View>
                            <Text style={[styles.aboutTxt, { paddingBottom: 16 }]}>Snap Parking</Text>
                            <Text style={styles.aboutTxt}>Version</Text>
                            <Text style={styles.version}>1.0</Text>
                        </View>
                    </View>
                    <View style={styles.row, { alignSelf: 'center', marginTop: 16 }}>
                        <TouchableOpacity style={styles.changePasswordBtn}
                            onPress={() => this.logOut()}>
                            <Image style={styles.btnLogo} source={require('../../assets/img/white_change-password.png')} />
                            <Text style={styles.txt}>Log Out</Text>
                        </TouchableOpacity>
                    </View>
                </ImageBackground>
            </View>
        )
    }
}