import React from "react";

import styles from "../../styles/LoginStyle";
import { Keyboard, Text, View, TextInput, TouchableWithoutFeedback, Image, TouchableOpacity, AsyncStorage } from 'react-native';
import * as Constants from '../../common/constants';

const appId = "1047121222092614"

export default class LoginScreen extends React.Component {
  state = {
    // token: '',
    username: '',
    password: '',
    mess: '',
    status: false
  }
  storeToken = async (token) => {
    try {
      await AsyncStorage.setItem('@storage_param', JSON.stringify({ 'token': token }))
      console.log(token)
    } catch (e) {
      // saving error
    }
  }
  componentDidMount() {
    this.checkToken();
  }


  checkToken = async () => {
    try {
      let param = await AsyncStorage.getItem('@storage_param');
      param = JSON.parse(param)
      if (param.token != undefined) {
        // this.props.navigation.navigate('HomePage');
        this.checkReservation();
      }
    } catch (e) {
      // error reading value
    }
  }
  render() {
    return (
      <View style={styles.containerView} behavior="padding">
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <View style={styles.loginScreenContainer}>
            <View style={styles.loginFormView}>
              <View style={styles.logoImageContainer}>
                <Image
                  style={styles.logoImage}
                  source={require('../../assets/img/ic_launcher.png')} />
                <Text style={styles.logoText}>Welcome!</Text>
                <Text style={styles.subText}>Join us for easy parking</Text>
              </View>
              <View style={styles.bottomView}>
                <TextInput placeholder="Username"
                  value={this.state.username}
                  selectionColor='#eeeeee'
                  onChangeText={(username) => this.setState({ username })}
                  placeholderColor="#fff"
                  style={styles.loginFormTextInput} />
                <TextInput placeholder="Password"
                  value={this.state.password}
                  selectionColor='#eeeeee'
                  onChangeText={(password) => this.setState({ password })}
                  placeholderColor="#fff"
                  style={styles.loginFormTextInput}
                  secureTextEntry={true} />
              </View>
              <View style={styles.loginButtonView}>
                <TouchableOpacity onPress={() => this.logIn()}>
                  <Image
                    style={styles.loginButton}
                    source={require('../../assets/img/arrow.png')} />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Register', { BASE_URL: this.state.BASE_URL })}>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={styles.newUser}>
                      Bạn chưa có tài khoản?
                    </Text>
                    <Text style={styles.signUp}>Đăng ký ngay </Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </TouchableWithoutFeedback>
        {this.state.status &&
          <View style={styles.message}>
            <Text style={[styles.subTxt, { color: '#eeeeee' }]}>{this.state.message || ' '}</Text>
          </View>}
      </View>
    );
  }

  checkReservation = async () => {
    let param = await AsyncStorage.getItem('@storage_param');
    param = JSON.parse(param);
    fetch(Constants.BASE_URL + '/parking/reservation/', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: param.token
      }
    })
      .then(response => response.json())
      .then(responseJson => {
        this.setState(
          {
            isLoading: false,
            dataSource_: responseJson,
          },
          function () {
            if (this.state.dataSource_) {
              if (this.state.dataSource_.result) {
                this.props.navigation.navigate('Direction');
              }
              else {
                this.props.navigation.navigate('HomePage');
              }
            }
          }
        );
      })
      .catch(error => {
        console.error(error);
      });
  }

  logIn = async => {
    fetch(Constants.BASE_URL + '/account/login/', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        username: this.state.username,
        password: this.state.password,
      })
    })
      .then(response => response.json())
      .then(responseJson => {
        this.setState(
          {
            isLoading: false,
            dataSource: responseJson,
            token: 'Token ' + responseJson.token || ''
          },
          function () {
            if (responseJson.token) {
              this.storeToken(this.state.token);
              this.checkReservation();
            }
            else {
              setTimeout(() => {
                this.setState({
                  message: 'Tài khoản hoặc mật khẩu không chính xác.',
                  status: true,
                })
              }, 2000)
              setTimeout(() => {
                this.setState({
                  status: false
                })
              }, 5000)
            }
          }
        );
      })
      .catch(error => {
        console.error(error);
      });
  }
  getReservation() {
    fetch(Constants.BASE_URL + '/parking/reservation/', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: this.state.token
      }
    })
      .then(response => response.json())
      .then(responseJson => {
        this.setState(
          {
            isLoading: false,
            dataSource: responseJson,
            location_name: responseJson.result.location_name,
            area_name: responseJson.result.area_name,
            space_name: responseJson.result.space_name
          },
        );
      })
      .catch(error => {
        console.error(error);
      });
  }
}
