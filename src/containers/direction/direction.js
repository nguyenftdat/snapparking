import React from 'react';
import { 
    View, 
    Text, 
    ActivityIndicator, 
    Image, 
    ScrollView, 
    ImageBackground, 
    AsyncStorage, 
    Dimensions, 
    Modal,
    BackHandler } from 'react-native';
import * as Permissions from 'expo-permissions';
import styles from '../../styles/Direction';
import * as Constants from '../../common/constants';
import MapView from '../../components/mapView/mapView';
import { TouchableHighlight, TouchableOpacity } from 'react-native-gesture-handler';
import ImageViewer from 'react-native-image-zoom-viewer';
import Header from '../../components/header/header';

export default class Direction extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isModelVisible: false,
        }
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }
    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }
    
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }
    
    handleBackButtonClick = () => {
        BackHandler.exitApp();
        return true;
    }
    
    componentDidMount() {
        
        this.getReservation();
    }

    ShowModalFunction(visible) {
        this.setState({ isModelVisible: false });
    }


    storeKeyValue = async (key, value) => {
        try {
            await AsyncStorage.setItem(`@storage_${key}`, value);
        } catch (e) {
            // saving error
        }
    }

    getModal(){
        this.setState({
            isModelVisible: true
        })
    }

    getReservation = async () => {
        let param = await AsyncStorage.getItem('@storage_param');
        param = JSON.parse(param);
        console.log(param);
        fetch(Constants.BASE_URL + '/parking/reservation/', {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: param.token
            }
        })
            .then(response => response.json())
            .then(responseJson => {
                this.setState(
                    {
                        isLoading: false,
                        dataSource: responseJson,
                        location_name: responseJson.result.location_name,
                        area_name: responseJson.result.area_name,
                        space_name: responseJson.result.space_name,
                        desLatitude: responseJson.result.latitude,
                        desLongitude: responseJson.result.longitude,
                        latitude: param.lat,
                        longitude: param.lng
                    },
                    function() {
                        // this.storeKeyValue('StatusReservation', true);
                        
                        this.getIndoorMap();
                    }
                );
            })
            .catch(error => {
                console.error(error);
            });
    }

    cancelReservation = async () => {
        await AsyncStorage.mergeItem('@storage_param', JSON.stringify({lat: this.state.latitude, lng: this.state.longitude, status: false}))
        let param = await AsyncStorage.getItem('@storage_param');
        param = JSON.parse(param);
        fetch(Constants.BASE_URL + '/parking/reservation/', {
            method: 'DELETE',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: param.token
            },
            body: JSON.stringify({
                location_name: this.state.location_name,
                area_name: this.state.area_name,
                space_name: this.state.space_name
            })
        })
            .then(response => response.json())
            .then(responseJson => {
                this.setState(
                    {
                        isLoading: false,
                        dataSource_: responseJson,
                    },
                    function () {
                        // console.log(this.state.dataSource_)
                        if (responseJson.message) {
                            this.props.navigation.navigate('CarParking');
                        }
                    }
                );
            })
            .catch(error => {
                console.error(error);
            });
    }

    getIndoorMap = async () => {
        let param = await AsyncStorage.getItem('@storage_param');
        param = JSON.parse(param);
        fetch(Constants.BASE_URL + '/parking/indoor/map/', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: param.token
            },
            body: JSON.stringify({
                location_name: this.state.location_name,
                area_name: this.state.area_name,
                space_name: this.state.space_name
            })
        })
            .then(response => response.json())
            .then(responseJson => {
                this.setState(
                    {
                        isLoading: false,
                        indoorMap: responseJson,
                    },
                    function () {
                        // console.log(responseJson)
                    }
                );
            })
            .catch(error => {
                console.error(error);
            });
    }

    render() {
        // console.log(this.state)
        const { dataSource, indoorMap } = this.state
        if (this.state.indoorMap) {
            return (
                <ImageBackground source={require('../../assets/img/abstract_background_5.jpg')} style={{ flex: 1, resizeMode: 'contain' }}>
                    <Header navigation={this.props.navigation}></Header>
                    <ScrollView style={styles.container}>
                        <View style={styles.infoContainer}>
                            <Text style={styles.titleTxt}>Thông tin vị trí đặt chỗ</Text>
                            <View style={styles.infoBlock}>
                                <Text style={styles.leftTxt}>Địa chỉ</Text>
                                <Text style={styles.rightTxt}>{dataSource.result.address}</Text>
                            </View>
                            <View style={styles.infoBlock}>
                                <Text style={styles.leftTxt}>Khu vực</Text>
                                <Text style={styles.rightTxt}>{dataSource.result.area_name}</Text>
                            </View>
                            <View style={styles.infoBlock}>
                                <Text style={styles.leftTxt}>Vị trí</Text>
                                <Text style={styles.rightTxt}>{dataSource.result.space_name}</Text>
                            </View>
                            <View style={styles.infoBlock}>
                                <Text style={styles.leftTxt}>Giá</Text>
                                <Text style={styles.rightTxt}>{dataSource.result.price + ' vnđ'}</Text>
                            </View>
                            <View style={styles.qrcodeContainer}>
                                <Image
                                    style={{
                                        width: 200,
                                        height: 200,
                                        resizeMode: 'contain',
                                        alignSelf: 'center'
                                    }}
                                    source={{
                                        uri:
                                            `data:image/png;base64,${dataSource.result.qrcode}`,
                                    }}
                                />
                            </View>
                            <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                                <TouchableOpacity style={styles.cancelBtn} onPress={() => this.cancelReservation()}>
                                    <Text style={styles.cancelTxt}>HỦY</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.indoorMap} onPress={() => this.getModal()}>
                                    <Text style={styles.indoorTxt}> XEM VỊ TRÍ</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <MapView lat={this.state.latitude} lng={this.state.longitude} desLat={this.state.desLatitude} desLng={this.state.desLongitude} />
                    </ScrollView>
                    <Modal
                        visible={this.state.isModelVisible}
                        transparent={false}
                        onRequestClose={() => this.ShowModalFunction()}>
                        <ImageViewer style={{
                            width: Dimensions.get('window').width,
                            height: Dimensions.get('window').height,
                            resizeMode: 'contain',
                            alignSelf: 'center'
                        }}
                            imageUrls={[{
                                url: 
                                    `data:image/png;base64,${this.state.indoorMap.image}`,
                                
                            }]} />
                    </Modal>
                </ImageBackground>
            );
        }
        return (
            <View style={{ flex: 1, padding: 20 }}>
                <ActivityIndicator />
            </View>
        )
    }
}