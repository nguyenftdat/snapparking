import React from 'react';
import {
    View,
    Text,
    Image,
    ImageBackground,
    TextInput,
    UIManager,
    LayoutAnimation,
    TouchableOpacity,
    AsyncStorage,
    ActivityIndicator,
} from 'react-native';
import styles from '../../styles/History';
import Header from '../../components/header/header';
import Icon from 'react-native-vector-icons/MaterialIcons';
import * as Constants from '../../common/constants';
import { ScrollView } from 'react-native-gesture-handler';

if (Platform.OS === 'android' && UIManager.setLayoutAnimationEnabledExperimental) {
    UIManager.setLayoutAnimationEnabledExperimental(true);
}

export default class History extends React.Component {
    state = {

    }
    componentDidMount() {
        this.getHistory();
    }
    getHistory = async () => {
        let param = await AsyncStorage.getItem('@storage_param');
        param = JSON.parse(param);
        fetch(Constants.BASE_URL + '/parking/booking/history/', {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: param.token
            },
        })
            .then(response => response.json())
            .then(responseJson => {
                this.setState(
                    {
                        isLoading: false,
                        dataSource: responseJson,
                        // mode: null
                    },
                    function () {
                        console.log(this.state.dataSource)
                    }
                );
            })
            .catch(error => {
                console.error(error);
            });
    }
    render() {
        return (
            <ImageBackground source={require('../../assets/img/abstract_background_5.jpg')} style={{ flex: 1, resizeMode: 'contain' }}>
                <ScrollView>
                    <Header navigation={this.props.navigation}></Header>
                    {this.state.dataSource &&
                        <View style={styles.container}>
                            {this.state.dataSource.map(item => (
                                <View style={styles.block}>
                                    <View style={styles.leftBlock}>
                                        <Text style={styles.boldTxt}>{item.start_time.split('/')[0].trim()}</Text>
                                        {item.start_time.split('/')[1].trim() == '01' &&
                                            <Text style={styles.boldTxt}>Tháng 1</Text>
                                            || item.start_time.split('/')[1].trim() == '02' &&
                                            <Text style={styles.boldTxt}>Tháng 2</Text>
                                            || item.start_time.split('/')[1].trim() == '03' &&
                                            <Text style={styles.boldTxt}>Tháng 3</Text>
                                            || item.start_time.split('/')[1].trim() == '04' &&
                                            <Text style={styles.boldTxt}>Tháng 4</Text>
                                            || item.start_time.split('/')[1].trim() == '05' &&
                                            <Text style={styles.boldTxt}>Tháng 5</Text>
                                            || item.start_time.split('/')[1].trim() == '06' &&
                                            <Text style={styles.boldTxt}>Tháng 6</Text>
                                            || item.start_time.split('/')[1].trim() == '07' &&
                                            <Text style={styles.boldTxt}>Tháng 7</Text>
                                            || item.start_time.split('/')[1].trim() == '08' &&
                                            <Text style={styles.boldTxt}>Tháng 8</Text>
                                            || item.start_time.split('/')[1].trim() == '09' &&
                                            <Text style={styles.boldTxt}>Tháng 9</Text>
                                            || item.start_time.split('/')[1].trim() == '10' &&
                                            <Text style={styles.boldTxt}>Tháng 10</Text>
                                            || item.start_time.split('/')[1].trim() == '11' &&
                                            <Text style={styles.boldTxt}>Tháng 11</Text>
                                            || item.start_time.split('/')[1].trim() == '12' &&
                                            <Text style={styles.boldTxt}>Tháng 12</Text>}
                                    </View>
                                    <View style={styles.rightBlock}>
                                        <Text style={[styles.txt, {fontFamily: 'UTM-Swiss-CondensedBold'}]}>{item.location_name}</Text>
                                        <Text style={styles.txt}>{item.area_name + '    Vị trí: ' + item.space_name}</Text>
                                        <Text style={styles.txt}>{'Giá: ' + item.price + ' vnđ'}</Text>
                                        {item.end_time == 'undefined' &&
                                            <Text style={styles.txt}>{'Thời gian: ' + item.start_time.split(' ')[1].trim()}</Text>
                                            || <Text style={styles.txt}>{'Thời gian: ' + item.start_time.split(' ')[1].trim() + ' - ' + item.end_time.split(' ')[1].trim()}</Text>}
                                    </View>
                                </View>
                            ))}
                        </View>}
                </ScrollView>
            </ImageBackground>
        );
        // return (
        //     <View style={{ flex: 1, padding: 20 }}>
        //         <ActivityIndicator />
        //     </View>
        // );
    }
}