import React from 'react';
import {
    View,
    Text,
    ActivityIndicator,
    Modal,
    Dimensions
} from 'react-native';

import Header from '../../components/header/header';
import ImageViewer from 'react-native-image-zoom-viewer';


export default class ImageView extends React.Component {
    state = {
        indoorMap: this.props.navigation.state.params.indoorMap || null,
        isModelVisible: true,
    }
    ShowModalFunction(visible) {
        this.setState({ isModelVisible: false });
    }
    render() {
        if (!this.state.indoorMap) {
            return (
                <View style={{ flex: 1, padding: 20 }}>
                    <ActivityIndicator />
                </View>
            );
        }
        return (
            <View>
                <View>
                    <Modal
                        visible={this.state.isModelVisible}
                        transparent={false}
                        onRequestClose={() => this.ShowModalFunction()}>
                        <ImageViewer style={{
                            width: Dimensions.get('window').width,
                            height: Dimensions.get('window').height,
                            resizeMode: 'contain',
                            alignSelf: 'center'
                        }}
                            imageUrls={[{
                                url: 
                                    `data:image/png;base64,${this.state.indoorMap.image}`,
                                
                            }]} />
                    </Modal>
                </View>
            </View>
        );
    }
}