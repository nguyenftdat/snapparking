import React from 'react';
import { View, Text, ActivityIndicator, Image, ScrollView, ImageBackground, AsyncStorage } from 'react-native';
import MapView, { Marker, Polyline } from 'react-native-maps';
import * as Permissions from 'expo-permissions';
import styles from '../../styles/BookingStyle';
import Polylines from '@mapbox/polyline';
import * as Constants from '../../common/constants';
import { TouchableOpacity } from 'react-native-gesture-handler';

export default class Direction extends React.Component {
    state = {
        latitude: this.props.navigation.state.params.startLat || null,
        longitude: this.props.navigation.state.params.startLng || null,
        status: false,
        data: this.props.navigation.state.params.dataSource,
        check: false
    };

    async componentDidMount() {
        const { status } = await Permissions.getAsync(Permissions.LOCATION)
        if (status !== 'granted') {
            const response = await Permissions.askAsync(Permissions.LOCATION)
        }
        navigator.geolocation.getCurrentPosition(
            ({ coords: { latitude, longitude } }) => this.setState({ status: this.setStatus(latitude, this.state.latitude) }),
            (error) => console.log('Error', error)
        )
        this.setState({
            desLatitude: this.props.navigation.state.params.lat,
            desLongitude: this.props.navigation.state.params.lng
        }, this.mergeCoords)
        await this.getLocationStatictis(this.state.data.location_name)
    }

    getLocationStatictis = async (location_name) => {
        let param = await AsyncStorage.getItem('@storage_param');
        param = JSON.parse(param);
        fetch(Constants.BASE_URL + '/parking/locations/info/', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: param.token

            },
            body: JSON.stringify({
                location_name: location_name
            })
        })
            .then(response => response.json())
            .then(responseJson => {
                responseJson.areas.forEach(area => {
                    area.spaces.forEach(space => {
                        space.isSelected = false;
                    })
                });
                this.setState(
                    {
                        isLoading: false,
                        dataSource: responseJson,
                    },
                    function(){
                        // console.log(responseJson)
                    }
                );
            })
            .catch(error => {
                console.error(error);
            });
    }

    setStatus(lat, curLat) {
        console.log(lat, 'Latitude: ', curLat)
        // console.log(Math.abs(Number(lat-curLat)))
        if (Math.abs(Number(lat-curLat)) < 0.0009) {
            return true
        } return false
    }

    selectSpace(item_, item) {
        this.setState({
            space_name: item_.space_name,
            area_name: item.area_name,
            check: true
        })
        this.state.dataSource.areas.forEach(area => {
            area.spaces.forEach(space => {
                if (space.space_name == item_.space_name) {
                    space.isSelected = true;
                }
                else space.isSelected = false;
            })
        });
    }

    verifyBooking = async () => {
        await AsyncStorage.mergeItem('@storage_param', JSON.stringify({lat: this.state.latitude, lng: this.state.longitude, status: true}))
        let param = await AsyncStorage.getItem('@storage_param');
        param = JSON.parse(param);
        fetch(Constants.BASE_URL + '/parking/reservation/', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: param.token

            },
            body: JSON.stringify({
                location_name: this.state.data.location_name,
                area_name: this.state.area_name,
                space_name: this.state.space_name
            })
        })
            .then(response => response.json())
            .then(responseJson => {
                this.setState(
                    {
                        isLoading: false,
                        dataSource_: responseJson,
                    },
                    function () {
                        this.props.navigation.navigate('Direction');
                    }
                );
            })
            .catch(error => {
                console.error(error);
            });
    }

    mergeCoords = () => {
        const {
            latitude,
            longitude,
            desLatitude,
            desLongitude
        } = this.state
        const hasStartAndEnd = latitude !== null && desLatitude !== null

        if (hasStartAndEnd) {
            const concatStart = `${latitude},${longitude}`
            const concatEnd = `${desLatitude},${desLongitude}`
            this.getDirections(concatStart, concatEnd)

        }
    }

    async getDirections(startLoc, desLoc) {
        try {
            const resp = await fetch(`https://maps.googleapis.com/maps/api/directions/json?origin=${startLoc}&destination=${desLoc}&key=${Constants.API_key}`)
            const respJson = await resp.json();
            // console.log(respJson)
            const response = respJson.routes[0]
            const distanceTime = response.legs[0]
            const distance = distanceTime.distance.text
            const time = distanceTime.duration.text
            const points = Polylines.decode(respJson.routes[0].overview_polyline.points);
            const coords = points.map(point => {
                return {
                    latitude: point[0],
                    longitude: point[1]
                }
            })
            this.setState({ coords, distance, time })
        } catch (error) {
            console.log('Error: ', error);
        }
    }

    onMarkerPress = location => () => {
        const { coords: { latitude, longitude } } = location
        this.setState({
            destination: location,
            desLatitude: latitude,
            desLongitude: longitude
        }, this.mergeCoords)
    }

    renderMarkers = () => {
        // const locations = { latitude: this.props.navigation.state.params.lat, longitude: this.props.navigation.state.params.lng }
        // console.log('locations: ', locations)
        return (
            <View>
                {
                    <Marker
                        // key={idx}
                        coordinate={{ latitude: this.props.navigation.state.params.lat, longitude: this.props.navigation.state.params.lng }}
                    // onPress={this.onMarkerPress(location)}
                    />
                }
            </View>
        )
    }


    render() {
        // const { latitude, longitude, coords } = this.state
        // console.log(this.state)
        const latitude = this.state.latitude
        const longitude = this.state.longitude;
        const coords = this.state.coords;
        console.log(this.state)
        // const {latitude, longitude} = this.state;
        if (coords) {
            return (
                <ScrollView style={styles.container}>
                    <ImageBackground source={require('../../assets/img/abstract_background_5.jpg')} style={{ flex: 1, resizeMode: 'contain' }}>
                        <View style={styles.infoContainer}>
                            <Text style={styles.name}>{this.state.data.location_name}</Text>
                            <View style={styles.row}>
                                <Text style={styles.leftTxt}>Địa chỉ</Text>
                                <Text style={styles.rightTxt}>{this.state.data.address}</Text>
                            </View>
                            <View style={styles.row}>
                                <Text style={styles.leftTxt}>Vị trí trống/Tổng số</Text>
                                <Text style={styles.rightTxt}>{this.state.data.stat.EMPTY + '/' + Number(this.state.data.stat.EMPTY + this.state.data.stat.FILLED)}</Text>
                            </View>
                            <View style={styles.row}>
                                <Text style={styles.leftTxt}>Thời gian hoạt động</Text>
                                {this.state.data.open_time && 
                                <Text style={styles.rightTxt}>{this.state.data.schedule.open_time + ' - ' + this.state.data.schedule.close_time}</Text>}
                            </View>
                            <View style={styles.row}>
                                <Text style={styles.leftTxt}>Giá</Text>
                                <Text style={styles.rightTxt}>{this.state.data.min_price + ' - ' + this.state.data.max_price + ' vnđ'}</Text>
                            </View>
                            <View style={styles.row}>
                                <Text style={styles.leftTxt}>Khoảng cách</Text>
                                <Text style={styles.rightTxt}>{this.state.data.distance + ' km'}</Text>
                            </View>
                            <View style={styles.row}>
                                <Text style={styles.leftTxt}>Thời gian di chuyển</Text>
                                <Text style={styles.rightTxt}>{this.state.data.duration + ' mins'}</Text>
                            </View>
                        </View>
                        <View>
                            <MapView
                                style={styles.mapView}
                                showsUserLocation
                                showsMyLocationButton={true}
                                initialRegion={{
                                    latitude,
                                    longitude,
                                    latitudeDelta: 0.0922,
                                    longitudeDelta: 0.0421,
                                }}>
                                {!this.state.status &&
                                    <Marker coordinate={{ latitude: this.props.navigation.state.params.startLat, longitude: this.props.navigation.state.params.startLng }}
                                    >
                                        <Image
                                            source={require('../../assets/img/location_icon.png')}
                                            style={{ width: 30, height: 30 }}
                                            resizeMode="contain"
                                        />
                                    </Marker>}
                                {this.renderMarkers()}
                                <Polyline
                                    strokeWidth={2}
                                    strokeColor="red"
                                    coordinates={coords}
                                />
                            </MapView>
                        </View>
                        <View style={styles.area}>
                            <Text style={styles.leftTxt}>CHỌN VỊ TRÍ</Text>
                            {this.state.dataSource &&
                                <View>
                                    {this.state.dataSource.areas.map(item => (
                                        <View>
                                            <Text style={styles.txt}>{item.area_name + ' - ' + item.price + ' vnđ'}</Text>
                                            <View style={styles.row}>
                                                {item.spaces.map(item_ => (
                                                    item_.status == 'EMPTY' && item_.isSelected == false &&
                                                    <TouchableOpacity style={styles.emptySpace}
                                                        onPress={() => this.selectSpace(item_, item)}>
                                                        <Text style={styles.whiteTxt}>{item_.space_name}</Text>
                                                    </TouchableOpacity>
                                                    || item_.status == 'FILLED' &&
                                                    <TouchableOpacity style={styles.filledSpace}>
                                                        <Text style={styles.whiteTxt}>{item_.space_name}</Text>
                                                    </TouchableOpacity>
                                                    || item_.status == 'EMPTY' && item_.isSelected == true &&
                                                    <TouchableOpacity style={styles.selectedSpace}
                                                    >
                                                        <Text style={styles.whiteTxt}>{item_.space_name}</Text>
                                                    </TouchableOpacity>
                                                ))}
                                            </View>
                                        </View>
                                    ))}

                                </View>}
                        </View>
                        {this.state.check &&
                            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                <TouchableOpacity style={styles.bookingBtn}
                                    onPress={() => this.verifyBooking()}>
                                    <Text style={styles.verifyTxt}>XÁC NHẬN</Text>
                                </TouchableOpacity>
                            </View>
                            || !this.state.check &&
                            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                <TouchableOpacity style={styles.noBookingBtn}>
                                    <Text style={styles.verifyTxt}>XÁC NHẬN</Text>
                                </TouchableOpacity>
                            </View>}
                    </ImageBackground>
                </ScrollView>
            );
        }
        return (
            <View style={{ flex: 1, padding: 20 }}>
                <ActivityIndicator />
            </View>
        );
    }
}